//
//  ChessApp.swift
//  Chess
//
//  Created by Manuel Meyer on 22.05.22.
//

import SwiftUI

@main
struct ChessApp: App {
    var body: some Scene {
        WindowGroup {
            ChessView(chess: Chess.createPopulatedBoard())
        }
    }
}
