//
//  ContentView.swift
//  Chess
//
//  Created by Manuel Meyer on 22.05.22.
//

import SwiftUI

struct ChessView: View {
    @State var chess: Chess.Board
    var body: some View {
        VStack {
            Text("\(chess.description.trimmingCharacters(in: .whitespacesAndNewlines))")
                .font(.system(.largeTitle, design:.monospaced))
                .padding()
                .fixedSize()
        }
    }
}
