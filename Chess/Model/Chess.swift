//
//  Chess.swift
//  Chess
//
//  Created by Manuel Meyer on 22.05.22.
//

enum Chess {
    enum MoveDescriptor {
        case move(Piece, from:Coordinate, to:Coordinate)
    }
    typealias Move = (MoveDescriptor, () -> ())
    typealias Coordinate = (v:Int,h:Int)
    enum Player:Equatable { case white, black }
    enum Piece:Equatable {
        case pawn  (Player)
        case rook  (Player)
        case knight(Player)
        case bishop(Player)
        case queen (Player)
        case king  (Player)
    }
    struct Board: CustomStringConvertible {
        fileprivate let fields: [Field]
        var description: String { sortedFields(for:self).enumerated().reduce("") { $0 + "\(charFor(field:$1.element))" + (($1.offset % 8 == 7) ? "\n" : " ") } + "\(allMoves.count)" }
    }
    static func createPopulatedBoard() -> Board {
        [   addWhitePawns  /*♙*/, addBlackPawns  /*♟*/,
            addWhiteRooks  /*♖*/, addBlackRooks  /*♜*/,
            addWhiteKnights/*♘*/, addBlackKnights/*♞*/,
            addWhiteBishops/*♗*/, addBlackBishops/*♝*/,
            addWhiteQueen  /*♕*/, addBlackQueen  /*♛*/,
            addWhiteKing   /*♔*/, addBlackKing   /*♚*/
        ].reduce(Chess.createEmptyBoard()) { $1($0) }
    }
    static fileprivate
    let allMoves =
        allPossibleWhitePawnMoveDescriptions()
      + allPossibleBlackPawnMoveDescriptions()
      + allPossibleWhiteRookMoveDescriptions()
      + allPossibleBlackRookMoveDescriptions()
      + allPossibleWhiteKnightMoveDescriptions()
      + allPossibleBlackKnightMoveDescriptions()
      + allPossibleWhiteBishopMoveDescriptions()
      + allPossibleBlackBishopMoveDescriptions()
      + allPossibleWhiteQueenMoveDescriptions()
      + allPossibleBlackQueenMoveDescriptions()
      + allPossibleWhiteKingMoveDescriptions()
      + allPossibleBlackKingMoveDescriptions()
}
fileprivate extension Chess.Board {
     enum Change {
        case update(Update); enum Update {
            case field(with:Chess.Field)
        }
    }
    func alter(_ cs:Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    func alter(_ cs:[Change]) -> Self { cs.reduce(self) { $0.alter($1) } }
}
private extension Chess.Board {
    func alter(_ c:Change) -> Self {
        switch c {
        case let .update(.field(with:field)):
            return .init(fields:fields.filter{ $0.coordinate.v != field.coordinate.v || $0.coordinate.h != field.coordinate.h } + [field])
        }
    }
}
fileprivate extension Chess {
    struct Field {
        let coordinate: Coordinate
        let value:Value
        enum Value:Equatable {
            case empty
            case occupied(with:Piece)
        }
    }
    static func allCoordiantes() -> [Coordinate] { (0..<64).map { (v:$0/8,h:$0%8) } }
    static func createEmptyBoard() -> Board { .init(fields:allCoordiantes().map { Field(coordinate:$0, value:.empty) }) }
    static func allPossibleWhitePawnMoveDescriptions() -> [MoveDescriptor] {
          (0..<64).map { .move(.pawn(.white),from:($0/8,$0%8),to:($0/8+1,$0%8  )) }
        + (0..<64).map { .move(.pawn(.white),from:($0/8,$0%8),to:($0/8+2,$0%8  )) }
        + (0..<64).map { .move(.pawn(.white),from:($0/8,$0%8),to:($0/8+1,$0%8-1)) }
        + (0..<64).map { .move(.pawn(.white),from:($0/8,$0%8),to:($0/8+1,$0%8+1)) }
    }
    static func allPossibleBlackPawnMoveDescriptions() -> [MoveDescriptor] {
          (0..<64).map { .move(.pawn(.black),from:($0/8,$0%8),to:($0/8-1,$0%8  )) }
        + (0..<64).map { .move(.pawn(.black),from:($0/8,$0%8),to:($0/8-2,$0%8  )) }
        + (0..<64).map { .move(.pawn(.black),from:($0/8,$0%8),to:($0/8-1,$0%8-1)) }
        + (0..<64).map { .move(.pawn(.black),from:($0/8,$0%8),to:($0/8-1,$0%8+1)) }
    }
    static func allPossibleWhiteRookMoveDescriptions  () -> [MoveDescriptor] { moveRook0  (.white) + moveRook1  (.white) + moveRook2  (.white) + moveRook3  (.white) }
    static func allPossibleBlackRookMoveDescriptions  () -> [MoveDescriptor] { moveRook0  (.black) + moveRook1  (.black) + moveRook2  (.black) + moveRook3  (.black) }
    static func allPossibleWhiteBishopMoveDescriptions() -> [MoveDescriptor] { moveBishop0(.white) + moveBishop1(.white) + moveBishop2(.white) + moveBishop3(.white) }
    static func allPossibleBlackBishopMoveDescriptions() -> [MoveDescriptor] { moveBishop0(.black) + moveBishop1(.black) + moveBishop2(.black) + moveBishop3(.black) }
    static func allPossibleWhiteKnightMoveDescriptions() -> [MoveDescriptor] {
          (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8+2,$0%8+1)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8+2,$0%8-1)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8-2,$0%8+1)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8-2,$0%8-1)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8+1,$0%8+2)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8+1,$0%8-2)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8-1,$0%8+2)) }
        + (0..<64).map { .move(.knight(.white),from:($0/8,$0%8),to:($0/8-1,$0%8-2)) }
    }
    static func allPossibleBlackKnightMoveDescriptions() -> [MoveDescriptor] {
          (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8+2,$0%8+1)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8+2,$0%8-1)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8-2,$0%8+1)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8-2,$0%8-1)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8+1,$0%8+2)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8+1,$0%8-2)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8-1,$0%8+2)) }
        + (0..<64).map { .move(.knight(.black),from:($0/8,$0%8),to:($0/8-1,$0%8-2)) }
    }
    static func allPossibleWhiteQueenMoveDescriptions() -> [MoveDescriptor] { moveQueen0(.white) + moveQueen1(.white) + moveQueen2(.white) + moveQueen3(.white) + moveQueen4(.white) + moveQueen5(.white) + moveQueen6(.white) + moveQueen7(.white) }
    static func allPossibleBlackQueenMoveDescriptions() -> [MoveDescriptor] { moveQueen0(.black) + moveQueen1(.black) + moveQueen2(.black) + moveQueen3(.black) + moveQueen4(.black) + moveQueen5(.black) + moveQueen6(.black) + moveQueen7(.black) }
    static func allPossibleWhiteKingMoveDescriptions () -> [MoveDescriptor] {
           (0..<64).map { .move(.king(.white),from:($0/8,$0/8  ),to:($0/8,$0/8+1)) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8  ),to:($0/8,$0/8-1)) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8+1),to:($0/8,$0/8  )) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8-1),to:($0/8,$0/8  )) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8+1),to:($0/8,$0/8+1)) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8+1),to:($0/8,$0/8-1)) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8-1),to:($0/8,$0/8+1)) }
        +  (0..<64).map { .move(.king(.white),from:($0/8,$0/8-1),to:($0/8,$0/8-1)) }
    }
    static func allPossibleBlackKingMoveDescriptions () -> [MoveDescriptor] {
           (0..<64).map { .move(.king(.black),from:($0/8,$0/8  ),to:($0/8,$0/8+1)) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8  ),to:($0/8,$0/8-1)) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8+1),to:($0/8,$0/8  )) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8-1),to:($0/8,$0/8  )) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8+1),to:($0/8,$0/8+1)) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8+1),to:($0/8,$0/8-1)) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8-1),to:($0/8,$0/8+1)) }
        +  (0..<64).map { .move(.king(.black),from:($0/8,$0/8-1),to:($0/8,$0/8-1)) }
    }
}
private extension Chess {
    static func moveRook0  (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.rook  (p),from:(c/8,c/8),to:(c/8   ,c/8+$0))} } }
    static func moveRook1  (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.rook  (p),from:(c/8,c/8),to:(c/8   ,c/8-$0))} } }
    static func moveRook2  (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.rook  (p),from:(c/8,c/8),to:(c/8+$0,c/8   ))} } }
    static func moveRook3  (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.rook  (p),from:(c/8,c/8),to:(c/8-$0,c/8   ))} } }
    static func moveBishop0(_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.bishop(p),from:(c/8,c/8),to:(c/8+$0,c/8+$0))} } }
    static func moveBishop1(_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.bishop(p),from:(c/8,c/8),to:(c/8+$0,c/8-$0))} } }
    static func moveBishop2(_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.bishop(p),from:(c/8,c/8),to:(c/8-$0,c/8+$0))} } }
    static func moveBishop3(_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.bishop(p),from:(c/8,c/8),to:(c/8-$0,c/8-$0))} } }
    static func moveQueen0 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8   ,c/8+$0))} } }
    static func moveQueen1 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8   ,c/8-$0))} } }
    static func moveQueen2 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8+$0,c/8   ))} } }
    static func moveQueen3 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8-$0,c/8   ))} } }
    static func moveQueen4 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8+$0,c/8+$0))} } }
    static func moveQueen5 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8+$0,c/8-$0))} } }
    static func moveQueen6 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8-$0,c/8+$0))} } }
    static func moveQueen7 (_ p:Player) -> [MoveDescriptor] { (0..<64).flatMap { c in (0..<8).map {.move(.queen (p),from:(c/8,c/8),to:(c/8-$0,c/8-$0))} } }
}
fileprivate
func charFor(field: Chess.Field) -> String.Element {
    switch field.value {
    case .empty                         : return " "
        // white
    case .occupied(with:.pawn  (.white)): return "♙"
    case .occupied(with:.rook  (.white)): return "♖"
    case .occupied(with:.knight(.white)): return "♘"
    case .occupied(with:.bishop(.white)): return "♗"
    case .occupied(with:.queen (.white)): return "♕"
    case .occupied(with:.king  (.white)): return "♔"
        // black
    case .occupied(with:.pawn  (.black)): return "♟"
    case .occupied(with:.rook  (.black)): return "♜"
    case .occupied(with:.knight(.black)): return "♞"
    case .occupied(with:.bishop(.black)): return "♝"
    case .occupied(with:.queen (.black)): return "♛"
    case .occupied(with:.king  (.black)): return "♚"
    }
}
fileprivate
func addWhitePawns(to board:Chess.Board) -> Chess.Board {
    board.alter((0..<8).map { .update(.field(with:.init(coordinate:($0,1), value:.occupied(with:.pawn(.white))))) })
}
fileprivate
func addWhiteRooks(to board:Chess.Board) -> Chess.Board {
    board
        .alter(.update(.field(with:.init(coordinate:(0,0),value:.occupied(with:.rook(.white))))))
        .alter(.update(.field(with:.init(coordinate:(7,0),value:.occupied(with:.rook(.white))))))
}
fileprivate
func addWhiteKnights(to board:Chess.Board) -> Chess.Board {
    board
        .alter(.update(.field(with:.init(coordinate:(1,0),value:.occupied(with:.knight(.white))))))
        .alter(.update(.field(with:.init(coordinate:(6,0),value:.occupied(with:.knight(.white))))))
}
fileprivate
func addWhiteBishops(to board:Chess.Board) -> Chess.Board {
    board
        .alter(.update(.field(with:.init(coordinate:(2,0),value:.occupied(with:.bishop(.white))))))
        .alter(.update(.field(with:.init(coordinate:(5,0),value:.occupied(with:.bishop(.white))))))
}
fileprivate
func addWhiteKing(to board:Chess.Board) -> Chess.Board {
    board.alter(.update(.field(with:.init(coordinate:(3,0),value:.occupied(with:.king(.white))))))
}
fileprivate
func addWhiteQueen(to board:Chess.Board) -> Chess.Board {
    board.alter(.update(.field(with:.init(coordinate:(4,0),value:.occupied(with:.queen(.white))))))
}
fileprivate
func addBlackPawns(to board:Chess.Board) -> Chess.Board {
    board.alter((0..<8).map { .update(.field(with:.init(coordinate:($0,6),value:.occupied(with:.pawn(.black))))) })
}
fileprivate
func addBlackRooks(to board:Chess.Board) -> Chess.Board {
    board
        .alter(.update(.field(with:.init(coordinate:(0,7),value:.occupied(with:.rook(.black))))))
        .alter(.update(.field(with:.init(coordinate:(7,7),value:.occupied(with:.rook(.black))))))
}
fileprivate
func addBlackKnights(to board:Chess.Board) -> Chess.Board {
    board
        .alter(.update(.field(with:.init(coordinate:(1,7),value:.occupied(with:.knight(.black))))))
        .alter(.update(.field(with:.init(coordinate:(6,7),value:.occupied(with:.knight(.black))))))
}
fileprivate
func addBlackBishops(to board:Chess.Board) -> Chess.Board {
    board
        .alter(.update(.field(with:.init(coordinate:(2,7),value:.occupied(with:.bishop(.black))))))
        .alter(.update(.field(with:.init(coordinate:(5,7),value:.occupied(with:.bishop(.black))))))
}
fileprivate
func addBlackKing(to board:Chess.Board) -> Chess.Board {
    board.alter(.update(.field(with:.init(coordinate:(3,7), value:.occupied(with:.king(.black))))))
}
fileprivate
func addBlackQueen(to board:Chess.Board) -> Chess.Board {
    board.alter(.update(.field(with:.init(coordinate:(4,7),value:.occupied(with:.queen(.black))))))
}
fileprivate
func sortedFields(for board:Chess.Board) -> [Chess.Field] {
    board.fields.sorted {
        $0.coordinate.h == $1.coordinate.h
            ? $0.coordinate.v < $1.coordinate.v
            : $0.coordinate.h < $1.coordinate.h
    }
}
